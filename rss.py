# -*- coding: utf-8 -*-

import feedparser
import os
import json
import datetime
# import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import wordcloud as wc
import click
from bs4 import BeautifulSoup
import urllib2


matplotlib.style.use('ggplot')
# Avoid encoding errors with Python 3:
feedparser.PREFERRED_XML_PARSERS.remove('drv_libxml2')

records_folder = "data"


def remove_html_tag(string_):
    """If an html tag is found, returns text between the opening and closing
    tags. Useful for some titles where href tags can be found."""
    soup = BeautifulSoup(string_, 'html.parser')
    tags = soup.find_all()
    if tags:
        return '\n'.join([tag.string for tag in tags])
    else:
        return string_


def make_file_name(string_, extension=".xml"):
    file_name = "".join(x for x in string_ if x.isalnum())
    return file_name + extension


def file_is_older_than(path, age):
    """Returns True if file is older than age (in minutes)"""
    modif_dt = os.path.getmtime(path)
    modif_dt = datetime.datetime.fromtimestamp(modif_dt)
    now_dt = datetime.datetime.now()
    file_age = (now_dt - modif_dt).total_seconds() / 60.
    return file_age > age


class Entry:
    """Represents a single feed entry"""

    def __init__(self, raw_entry):
        self.title = None
        self.summary = None
        self.link = None
        self.description = None
        self.update(raw_entry)

    def update(self, raw_entry):
        title = raw_entry.get("title", "")
        if not title:
            print("Warning: Could not find title key in feed entry")
        summary = raw_entry.get("summary", "")
        link = raw_entry.get("link", "")
        self.title = remove_html_tag(title)
        self.summary = summary
        self.link = link
        self.description = u"Title: {}\nLink: {}".format(self.title, self.link)

    def search(self, word):
        """
        Search a word (case-sensitive) in the entry's title.
        Returns entry's title if the word was found, None otherwise.
        """
        if word in self.title:
            return self.title
        else:
            return None


class Feed:
    """Represents a Feed source that has Entries"""

    def __init__(self, url, name=None, verbose=True):
        self.url = url
        self.name = name
        self.link = None
        self.description = None
        self.entries = []
        if self.name:
            # Make record file name from given feed name
            file_name = make_file_name(self.name)
            self.record_file = os.path.sep.join([records_folder, file_name])
        else:
            self.record_file = None
        self.verbose = verbose
        self.update()

    def update(self):
        """
        Updates feed attributes from a record file if existing and not too old
        otherwise parses feed content from URL
        """
        if self.record_file and os.path.exists(self.record_file):
            file_is_old = file_is_older_than(self.record_file, 60)
            if file_is_old:
                if self.verbose:
                    print("File {} is out of date".format(
                        self.record_file))
            else:
                # Read record file
                try:
                    with open(self.record_file, 'r') as rf:
                        raw_feed = feedparser.parse(rf.read())
                        self.set_attributes(raw_feed)
                        return
                except ValueError:
                    print("Warning: Could not parse file {}".format(self.record_file))
        # Parse feed
        if self.verbose: print("Getting {}...".format(self.url))
        raw_feed = feedparser.parse(self.url)
        self.set_attributes(raw_feed)
        # Write record file
        file_name = make_file_name(self.name)
        self.record_file = os.path.sep.join([records_folder, file_name])
        with open(self.record_file, 'w') as rf:
            raw_feed = urllib2.urlopen(self.url)
            raw_feed = raw_feed.read()
            rf.write(raw_feed)

    def set_attributes(self, raw_feed):
        """Updates class attributes from raw feed format"""
        self.name = raw_feed["feed"].get("title", "Name not found")
        self.entries = [Entry(raw_entry) for raw_entry in raw_feed["entries"]]
        self.link = raw_feed["feed"].get("link")
        self.description = "Name : {}\nLink: {}".format(self.name, self.link)

    def describe(self, show_entries=False):
        if show_entries:
            entries_desc = [entry.description for entry in self.entries]
            entries_desc = "---\n".join(entries_desc)
            print(self.description + "\n" + entries_desc)
        else:
            print(self.description)

    def search(self, word):
        """
        Search a word (case-sensitive) in all entries titles.
        Returns the list of entries where the word was found.
        """
        result = []
        for entry in self.entries:
            if entry.search(word):
                result.append(entry)
        return result


class FeedCollection:

    def __init__(self):
        # TODO: Add this one:
        # "https://www.aljazeera.com/xml/rss/all.xml"
        self.urls = []
        self.config_file = "feed_list.json"
        self.feeds = []
        self.update()

    def update(self, verbose=True):
        if verbose: print("Updating feeds ...")
        if not self.feeds:
            # Get feed URLs from config file
            if not os.path.exists(self.config_file):
                raise(FileNotFoundError, "{] not found".format(self.config_file))
            with open(self.config_file, 'r') as nf:
                url_dict = json.loads(nf.read())
            # Fetch and parse feed for each url
            for name, url in url_dict.items():
                self.feeds.append(Feed(url, name=name, verbose=verbose))
        else:
            # Re-fetch and parse each feed
            for feed in self.feeds:
                feed.update()

    def describe(self):
        print("Collection of {} feeds\n".format(len(self.feeds)))
        for feed in self.feeds:
            print("{}\n--\n".format(feed.description))

    def search(self, word):
        results = {}
        for feed in self.feeds:
            entries = feed.search(word)
            if entries:
                results[feed] = entries
        return results


def apply_filter(word, exceptions=None, min_length=None):
    """Applies filtering criteria, returns boolean.
    True, the word is accepted,
    False, the word should be ignored."""
    exceptions = exceptions or []
    if min_length and len(word) < min_length:
        return False
    if word.lower() in exceptions:
        return False
    return True


class WordCloud:
    """Make and show a word cloud"""

    def __init__(self, feed_col):
        self.words_to_ignore = ["what", "will", "with", "says", "know"]
        self.min_word_length = 3
        self.words_per_feed = []
        self.feed_collection = feed_col
        self.update(feed_col)

    def update(self, feed_col):
        self.feed_collection = feed_col
        self.cut_strings()

    def cut_strings(self):
        result = {}  # key: feed name, value: list of words present in titles
        for feed in self.feed_collection.feeds:
            result[feed.name] = []
            # For each title, cut the title into words
            for entry in feed.entries:
                for word in entry.title.split(' '):
                    # Filter irrelevant words
                    if apply_filter(word, exceptions=self.words_to_ignore,
                                    min_length=self.min_word_length):
                        result[feed.name].append(word)
        self.words_per_feed = result

    def show_cloud(self, detail=True, nb_cols=3, bg_color='white',
                   figsize=(15, 15)):
        w = wc.WordCloud(background_color=bg_color)
        words_per_feed = self.words_per_feed
        if detail:
            plt.figure("detail", figsize=figsize)
            nb_rows = int(len(words_per_feed) / float(nb_cols)) + 1
            index = 1
            for key, vals in words_per_feed.items():
                plt.subplot(nb_rows, nb_cols, index)
                plt.title(key)
                plt.axis('off')
                if len(vals) > 0:
                    w.generate(' '.join(vals))
                    plt.imshow(w, interpolation="bilinear")
                else:
                    print('Warning: Missing data for {}'.format(key))
                index += 1
            plt.tight_layout()
        all_words = []
        for vals in words_per_feed.values():
            all_words += list(vals)
        plt.figure("summary")
        w.generate(' '.join(all_words))
        plt.imshow(w, interpolation="bilinear")
        plt.axis('off')
        plt.tight_layout()
        plt.show()


@click.group()
def rss():
    """Play with RSS feeds contents"""
    global rss_
    rss_ = FeedCollection()


@rss.command()
@click.argument("word")
@click.option("--silent", is_flag=True, help="No display")
def search(word, silent):
    """Search a word in the feeds"""
    res = rss_.search(word)
    count = sum([len(vals) for vals in list(res.values())])
    if not silent: print('total : ' + str(count) + ' occurrences')
    if not silent:
        for feed, entries in res.items():
            print('---' + feed.name)
            print('\n'.join([entry.title for entry in entries]))
    return res


@rss.command()
@click.option("--key", help="Search in a selected key", default="title",
              type=click.Choice(['title', 'summary']))
def cloud(key):
    """Makes a word cloud from provided RSS feeds"""
    wc_ = WordCloud(rss_)
    wc_.show_cloud()


@rss.command(name='describe')
def describe_rss():
    """Displays retrieved RSS feeds"""
    rss_.describe()


# Requires Pandas
# def show_histogram(news_words):
#     df = pd.DataFrame(dict([(k, pd.Series(v))
#                               for k, v in wc_.words_per_feed.items()]))
#     plt.figure()
#     df[df.columns[0]].value_counts().plot(kind='bar')
#     fig, ax = plt.subplots()
#     for col in df.columns:
#         df[col].value_counts().plot(ax=ax, kind='bar', label=col)
#     plt.legend(loc='best')
#     plt.show()


if __name__ == '__main__':
    # fc = FeedCollection()
    # fc.search("book")
    rss()



