from setuptools import setup

setup(
    name='rsswatcher',
    author='Fanny Moïni',
    version='0.1',
    py_modules=['rss'],
    install_requires=[
        'Click',
        'matplotlib',
        'wordcloud',
        'feedparser',
        'dash',
        'pandas',
        'BeautifulSoup'
    ],
    entry_points='''
        [console_scripts]
        rss=rss:rss
    ''',
)