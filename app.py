# -*- coding: utf-8 -*-
import dash
import dash.dependencies as dd
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import wordcloud as wc
from io import BytesIO
import base64

import rss


# -------------------------------- Global configuration
colors = {
    'background': '#FFFFFF',
    'text': '#111111'
}

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

feed_collection = rss.FeedCollection()


# -------------------------------- Wordcloud functions
def plot_wordcloud(data):
    all_words = []
    for vals in data.values():
        all_words += list(vals)
    wc_ = wc.WordCloud(background_color=colors['background'], width=480,
                       height=360)
    wc_.generate(' '.join(all_words))
    return wc_.to_image()


@app.callback(dd.Output('image_wc', 'src'), [dd.Input('image_wc', 'id')])
def make_image(b):
    wc_ = rss.WordCloud(feed_collection)
    img = BytesIO()
    plot_wordcloud(data=wc_.words_per_feed).save(img, format='PNG')
    return 'data:image/png;base64,{}'.format(
        base64.b64encode(img.getvalue()).decode())


# -------------------------------- Search table functions

@app.callback(dd.Output('table', 'children'), [dd.Input('search', 'value')])
def generate_search_results_table(searched_word, max_rows=10):
    results = feed_collection.search(searched_word)
    dataframe = pd.DataFrame(columns=['Source', 'Title'])
    for feed, entries in results.items():
        for entry in entries:
            dataframe = dataframe.append({'Source': feed.name,
                                          'Title': entry.title,
                                          'Link': dcc.Link("Link to the article",
                                                           href=entry.link,
                                                           title="Fly, little bird, fly !")},
                                         ignore_index=True)
    if len(dataframe) == 0:
        return "No occurrence found for '{}'".format(searched_word)
    return html.Table([
        html.Thead(
            html.Tr([html.Th(col) for col in dataframe.columns])
        ),
        html.Tbody([
            html.Tr([
                html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
            ]) for i in range(min(len(dataframe), max_rows))
        ])
    ])


# -------------------------------- Layout
app.layout = html.Div(
    style={
        'backgroundColor': colors['background']},
    children=[
        html.H1(
            children='Hello !',
            style={
                'textAlign': 'center',
                'color': colors['text']}),

        html.Div(
            children='Welcome to your world news watchtower.\nYou rock.',
            style={
                'textAlign': 'center',
                'color': colors['text']}),

        html.Div([
            html.H2(
                children="Today's wordcloud",
                style={
                    'textAlign': 'center',
                    'color': colors['text']}),

            html.Img(
                id="image_wc")],

            style={
                'textAlign': 'center',
                'color': colors['text']}),

        html.Div([

            html.H4(
                children='Search word occurrences'),

            html.Label(
                'Search a word'),

            dcc.Input(
                id='search',
                value='Coronavirus',
                type='text'),

            html.Div(
                id='table',
                style={
                    'textAlign': 'center',
                    'color': colors['text'],
                    'margin-left': '25%',
                    'margin-right': '25%'}
            )],

            style={
                'textAlign': 'center',
                'color': colors['text']}),
    ])

if __name__ == '__main__':
    app.run_server(debug=True)
